FROM python:3.7

WORKDIR /app

COPY . .

USER root

WORKDIR /app/

#RUN pip install -r requirements.txt

#RUN pip install sklearn

EXPOSE 8000

#ENTRYPOINT [ "python" ] 

CMD [ "python3", "-m", "http.server" ]
